export const logout = function() {
  if (this.$socket.connected) {
    this.$socket.client.emit("leave", this.$store.state.user.username);
    this.$socket.client.disconnect();
  }
  this.$cookies.remove("tkn");
  this.$store.commit("removeUser");
  this.$router.replace("/login");
};

export const unauthorizedCheck = function(response) {
  if (response.status === 401) {
    logout.call(this);
  }
};

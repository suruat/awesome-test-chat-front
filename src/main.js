import Vue from "vue";
import VueCookies from "vue-cookies";
import VueSocketIOExt from "vue-socket.io-extended";
import io from "socket.io-client";

import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;

const socket = io("http://localhost:8888", { autoConnect: false });

// Enabling plugins
Vue.use(VueCookies);
VueCookies.config("7d");
Vue.use(VueSocketIOExt, socket);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
